package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.json.JSONObject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Properties;

@Service
@RequiredArgsConstructor
public class KafkaConsumer implements CommandLineRunner {
    private final static String TOPIC = "kafka-log-json";
    //private final static String BOOTSTRAP_SERVER = "localhost:9092";
     /** Foreign server example */ private final static String BOOTSTRAP_SERVER = "37.46.131.116:9092";
    private final LogReportService logReportService;



    @Override
    public void run(String... args) {

        Thread consumerThread = new Thread(() -> {
            final Consumer<String, String> consumer = createConsumer();
            Duration duration = Duration.ofSeconds(1);
            while (true) {
                final ConsumerRecords<String, String> consumerRecords = consumer.poll(duration);
                if(consumerRecords.count() == 0) {
                    continue;
                }
                consumerRecords.forEach(record -> {
                    String value = record.value();
                    try {
                        JSONObject jsonObject = new JSONObject(value);
                        logReportService.LogReportToElastic(jsonObject);
                    } catch (Exception e) {
                        logReportService.LogReportToElasticWrongFormat(value);
                    }

                });
                consumer.commitAsync();
            }
        });
        consumerThread.start();
    }

    private static Consumer<String, String> createConsumer() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaLogToElasticConsumer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        final Consumer<String, String> consumer = new org.apache.kafka.clients.consumer.KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;

    }
}
