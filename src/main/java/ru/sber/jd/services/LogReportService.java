package ru.sber.jd.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.LogProjectBitsDto;
import ru.sber.jd.dto.LogReportDto;
import ru.sber.jd.entities.LogReportEntity;
import ru.sber.jd.repositories.LogProjectBitsRepository;
import ru.sber.jd.repositories.LogReportRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class LogReportService {
    private final LogReportRepository logReportRepository;
    private final LogProjectBitsRepository logProjectBitsRepository;
    private final Logger logger = LoggerFactory.getLogger(LogReportService.class);

    public LogReportDto insert(LogReportDto dto) {
        LogReportEntity save = logReportRepository.save(mapToEntity(dto));
        return mapToDto(save);
    }

    public void insertAll(List<LogReportDto> dtos) {
        for (LogReportDto dto:dtos) {
            logReportRepository.save(mapToEntity(dto));
        }
    }

    public List<LogReportDto> getAll() {
        return StreamSupport.stream(logReportRepository.findAll().spliterator(), false).map(this::mapToDto).collect(Collectors.toList());
    }

    public LogReportDto getById(Integer id) {
        return mapToDto(logReportRepository.findById(id).orElse(new LogReportEntity()));
    }

    public void deleteById(Integer id) {
        logReportRepository.deleteById(id);
    }


    public String toStringForLog(LogReportDto dto) {
        String result = String.format("level= %s, project= %s, author= %s, time= %s, text= %s"
                , dto.getLevel(), dto.getProject(), dto.getAuthor(), dto.getTime(), dto.getText());
        return result;
    }

    public List<LogProjectBitsDto> getProjectBits() {
        return logProjectBitsRepository.getProjectBits();
    }

    public List<LogProjectBitsDto> getInProjectBits(String project) {
        return logProjectBitsRepository.getInProjectBits(project);
    }

    private LogReportEntity mapToEntity (LogReportDto dto) {
        return new LogReportEntity()
                .setId(dto.getId())
                .setLevel(dto.getLevel())
                .setAuthor(dto.getAuthor())
                .setProject(dto.getProject())
                .setText(dto.getText())
                .setTime(dto.getTime());
    }

    private LogReportDto mapToDto (LogReportEntity entity) {
        return new LogReportDto()
                .setId(entity.getId())
                .setAuthor(entity.getAuthor())
                .setLevel(entity.getLevel())
                .setProject(entity.getProject())
                .setText(entity.getText())
                .setTime(entity.getTime());
    }

    public void LogReportToElastic(JSONObject jsonObject) {
        LogReportDto logReportDto = new LogReportDto();
        try {
            JSONObject message = (JSONObject) jsonObject.get("message");
            logReportDto.setLevel(message.getString("level"));
            logReportDto.setProject(message.getString("project"));
            logReportDto.setAuthor(message.getString("author"));
            logReportDto.setTime(message.getString("time"));
            logReportDto.setText(message.getString("text"));
            logger.info(message.toString());
            insert(logReportDto);
        } catch (Exception e) {
            try {
                logger.info(jsonObject.getString("message"));
            } catch (Exception ex) {
                System.out.println(jsonObject.get("message"));
            }
        }
    }

    public void LogReportToElasticWrongFormat(String value) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String format = dtf.format(now);
        String preparedJson = String.format("{\"text\": \"%s %s\", \"level\": \"ERROR\", \"time\": \"%s\", \"project\": \"Incorrect message type\", \"author\": \"UNKNOWN\"}"
                , "Некорректный формат сообщения: ",value, format);
        JSONObject wrongJsonFormat = new JSONObject(preparedJson);
        LogReportDto logReportDto = new LogReportDto();
        logReportDto.setLevel(wrongJsonFormat.getString("level"));
        logReportDto.setProject(wrongJsonFormat.getString("project"));
        logReportDto.setAuthor(wrongJsonFormat.getString("author"));
        logReportDto.setTime(wrongJsonFormat.getString("time"));
        logReportDto.setText(wrongJsonFormat.getString("text"));
        insert(logReportDto);
        logger.error(wrongJsonFormat.toString());
    }

}
