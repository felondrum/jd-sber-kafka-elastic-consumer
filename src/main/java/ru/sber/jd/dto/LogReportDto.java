package ru.sber.jd.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Data
@RequiredArgsConstructor
@Accessors(chain = true)
public class LogReportDto {
    private Integer id;
    private String text;
    private String author;
    private String project;
    private String time;
    private String level;
}
