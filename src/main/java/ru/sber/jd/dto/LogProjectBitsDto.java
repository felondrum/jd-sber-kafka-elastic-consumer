package ru.sber.jd.dto;


import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import java.math.BigInteger;

@Data
@RequiredArgsConstructor
@Accessors(chain = true)
public class LogProjectBitsDto {
    String name;
    BigInteger bits;
}
