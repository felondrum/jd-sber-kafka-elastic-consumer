package ru.sber.jd.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.LogReportEntity;


@Repository
public interface LogReportRepository extends CrudRepository<LogReportEntity, Integer> {
}
