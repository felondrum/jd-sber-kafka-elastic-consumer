package ru.sber.jd.repositories;

import org.springframework.stereotype.Repository;
import ru.sber.jd.dto.LogProjectBitsDto;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LogProjectBitsRepository {
    final static String NATIVE_QUERY_GET_ALL_PROJECT_BITS =
            "select project, bits from " +
                    "(select project, count(project) bits from log_report_entity group by project) " +
                    "order by bits desc, project";

    final static String NATIVE_QUERY_GET_IN_PROJECT_BITS =
            "select text, bits from " +
                    "(select text, count(text) bits from log_report_entity where project=?1  group by text) " +
                    "order by bits desc, text";

    @PersistenceContext
    private EntityManager em;

    public List<LogProjectBitsDto> getProjectBits() {
        Query nativeQuery = em.createNativeQuery(NATIVE_QUERY_GET_ALL_PROJECT_BITS);
        List<Object[]> resultList = nativeQuery.getResultList();
        List<LogProjectBitsDto> logProjectBitsDtos = new ArrayList<>();
        for (Object[] bits:resultList) {
            String name = (String) bits[0];
            BigInteger bitsCount = (BigInteger) bits[1];
            LogProjectBitsDto logProjectBitsDto = new LogProjectBitsDto();
            logProjectBitsDto.setName(name).setBits(bitsCount);
            logProjectBitsDtos.add(logProjectBitsDto);
        }
        return logProjectBitsDtos;
    }

    public List<LogProjectBitsDto> getInProjectBits(String project) {
        Query nativeQuery = em.createNativeQuery(NATIVE_QUERY_GET_IN_PROJECT_BITS);
        nativeQuery.setParameter(1, project);
        List<Object[]> resultList = nativeQuery.getResultList();
        List<LogProjectBitsDto> logProjectBitsDtos = new ArrayList<>();
        for (Object[] bits:resultList) {
            String name = (String) bits[0];
            BigInteger bitsCount = (BigInteger) bits[1];
            LogProjectBitsDto logProjectBitsDto = new LogProjectBitsDto();
            logProjectBitsDto.setName(name).setBits(bitsCount);
            logProjectBitsDtos.add(logProjectBitsDto);
        }
        return logProjectBitsDtos;
    }

}
