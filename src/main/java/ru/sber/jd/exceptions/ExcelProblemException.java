package ru.sber.jd.exceptions;

public class ExcelProblemException extends RuntimeException {
    public ExcelProblemException(String message) {
        super(message);
    }
}
