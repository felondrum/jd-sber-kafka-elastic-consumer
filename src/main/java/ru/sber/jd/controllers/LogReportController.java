package ru.sber.jd.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.ExcelChartEnum;
import ru.sber.jd.ExcelChartPositionEnum;
import ru.sber.jd.ExcelChartSizeEnum;
import ru.sber.jd.ExcelGenerator;
import ru.sber.jd.dto.LogProjectBitsDto;
import ru.sber.jd.dto.LogReportDto;
import ru.sber.jd.exceptions.DataBaseUnavailableException;
import ru.sber.jd.exceptions.ExcelProblemException;
import ru.sber.jd.exceptions.NotFoundException;
import ru.sber.jd.services.LogReportService;
import ru.sber.jd.utils.MediaTypeUtils;

import javax.servlet.ServletContext;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/logreport")
public class LogReportController {

    @Autowired
    private ServletContext servletContext;

    private final LogReportService logReportService;
    @GetMapping
    public ResponseEntity findAll() {
        try {
            List<LogReportDto> all = logReportService.getAll();
            if(all.isEmpty()) {
                throw new NotFoundException("Данные с записью логов отсутствуют!");
            }
            return ResponseEntity.ok(all);
        } catch (NotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Возникли сложности с базой данных.");
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Integer id) {
        try {
            LogReportDto byId = logReportService.getById(id);
            if (byId.getId() == null) {
                throw new NotFoundException(String.format("Данные логирования с id %s не найдены!", id));
            }
            return ResponseEntity.ok(byId);
        } catch (NotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new DataBaseUnavailableException("Возникли сложности с базой данных.");
        }


    }


    @GetMapping("/download/all_project")
    public ResponseEntity findAllProjectBits() {


        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<LogProjectBitsDto> list;

        try {
            list = logReportService.getProjectBits();
            if (list.isEmpty()) {
                throw new NotFoundException("Информация о статистике отсутствует!");
            }
        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }
        ExcelGenerator excelGenerator = new ExcelGenerator(list, LogProjectBitsDto.class);
        excelGenerator.setWorkbookName("get_stat_by_projects");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChart(ExcelChartEnum.COL, "name", "bits");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);
        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }
    }

    @GetMapping("/download/in_project/{name}")
    public ResponseEntity findInProjectBits(@PathVariable String name) {

        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
        List<LogProjectBitsDto> list;

        try {
            list = logReportService.getInProjectBits(name);
            if (list.isEmpty()) {
                throw new NotFoundException(String.format("Информация о статистике проекта %s отсутствует!", name));
            }
        } catch (NotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataBaseUnavailableException("Проблемы с доступом к базе данных.");
        }
        ExcelGenerator excelGenerator = new ExcelGenerator(list, LogProjectBitsDto.class);
        excelGenerator.setWorkbookName("get_in_project_stat_by_name");
        excelGenerator.setWorksheetName("report");
        excelGenerator.setChart(ExcelChartEnum.BAR, "name", "bits");
        excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
        excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);
        try {
            byte[] data = excelGenerator.exportToDownload();
            ByteArrayResource resource = new ByteArrayResource(data);
            String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());

            return  ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                    .contentType(mediaType)
                    .contentLength(data.length)
                    .body(resource);
        } catch (Exception e) {
            throw new ExcelProblemException("При создании файла Excel произошел сбой! " + e.getMessage());
        }

    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity NotFoundInDb (NotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler(DataBaseUnavailableException.class)
    public ResponseEntity SQLProblems (DataBaseUnavailableException e) {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.getMessage());
    }

    @ExceptionHandler(ExcelProblemException.class)
    public ResponseEntity ExcelProblems (ExcelProblemException e) {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.getMessage());
    }

}
